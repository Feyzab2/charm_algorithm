import pandas as pd
from copy import copy


#######################################################################
################ Class for Data Pre-Processing ########################
#######################################################################
class DataPreprocessing:

    # the list of transactions in the dataset
    list_transaction = []

    # the number of transactions in the dataset
    tid_count = 0

    # read the data file
    def read_data_file(self, filename):

        with open(filename, 'r') as file:
            tid = 1
            # for each line in file
            # if the line is not equal to NULL 
            # then all the element will be added to the list of transactions with the same tid
            for line in file:
                # check the line is not equal to NULL
                if len(line.strip()) != 0:

                    # the list contains all of the elements in a line
                    line = line.strip().split(' ')

                    # for each element in a line, add this element to the list of transactions
                    # with the same tid
                    for element in line:
                        self.list_transaction.append({'tid' : tid, 'item': element})
                    
                    # increase the tid to move to the next line (the next transaction)
                    tid += 1
        
        # calculate the number of transactions in the dataset
        self.tid_count = tid - 1

    # find the dataframe which has 
    # the first column is the item
    # the second column is the list of tid in which the item occurs
    def transform_data(self):

        # convert the list of transactions to dataframe
        df = pd.DataFrame(self.list_transaction)
        
        # combine all the tid of transactions that an item occurs
        self.groupedItems = df.groupby(['item'])['tid'].apply(list)
        
        # convert the result to data frame 
        self.groupedItems = pd.DataFrame({'item': self.groupedItems.index, 
                                        'tid': self.groupedItems.values})
        
        self.groupedItems['item'] = self.groupedItems['item'].apply(lambda x: {x})

    # get all the itemsets that has support equal or greater than min support
    def get_frequent_items(self, min_support):
        return self.groupedItems[self.groupedItems['tid'].map(len) 
                                >= min_support / 100 * self.tid_count]



#######################################################################
################ Class for CHARM Algorithm ############################
#######################################################################
class CharmAlgorithm:
    def __init__(self, min_support, tid_count):

        # the result of the algorithm, which is the list of closed itemset
        self.result = pd.DataFrame(columns=['item', 'tid', 'support'])

        # the min support
        self.min_support = min_support / 100 * tid_count
        self.level = 0
    
    # Replace all itemset with the new itemset
    @staticmethod
    def replace(df, column, find, replace_item):
        for itemset in df.itertuples():
            if find <= itemset[column]:
                itemset[column].update(replace_item)
    
    def charm_property(self, itemset1, itemset2, items, new_item, new_tid):
        if len(new_tid) >= self.min_support:

            # in case support of itemset-1 is equal to support of itemset-2
            if set(itemset1[2]) == set(itemset2[2]):
                items = items[items['item'] != itemset2[1]]
                find = copy(itemset1[1])
                self.replace(items, 1, find, new_item)
                self.replace(self.items_tmp, 1, find, new_item)

            # in case the list tid of itemset-1 is subset of the list tid of itemset-2
            elif set(itemset1[2]).issubset(set(itemset2[2])):
                find = copy(itemset1[1])
                self.replace(items, 1, find, new_item)
                self.replace(self.items_tmp, 1 , find, new_item)
            
            # in case the list tid of itemset-2 is subset of the list tid of itemset-1
            elif set(itemset2[2]).issubset(set(itemset1[2])):
                items = items[items['item'] != itemset2[1]]
                self.items_tmp = self.items_tmp.\
                                append({'item': new_item, 'tid': new_tid}, ignore_index=True)

            
            # in case the list tid of itemset-1 and the list tid of itemset-2 are different
            elif set(itemset1[2]) != set(itemset2[2]):
                self.items_tmp = self.items_tmp.\
                                append({'item': new_item, 'tid': new_tid}, ignore_index=True)
    
    def charm_extend(self, items_grouped):

        # sort the itemset with increasing order of its support
        s = items_grouped.tid.str.len().sort_values().index
        items_grouped = items_grouped.reindex(s).reset_index(drop=True)

        # for each 1st itemset{item, tid, support} in the list of itemsets
        for itemset1 in items_grouped.itertuples():
            self.items_tmp = pd.DataFrame(columns=['item', 'tid'])

            # for each 2nd itemset, with 2nd itemset is greater than 1st itemset
            # for some dataset, this will be calculated in the alphabet
            for itemset2 in items_grouped.itertuples():
                if itemset2[0] >= itemset1[0]:
                    item = set()
                    item.update(itemset1[1])
                    item.update(itemset2[1])
                    tid = list(set(itemset1[2]) & set(itemset2[2]))
                    self.charm_property(itemset1, itemset2, items_grouped, item, tid)
            
            if not self.items_tmp.empty:
                self.charm_extend(self.items_tmp)
            
            # check if item subsumed
            is_subsumption = False
            for row in self.result.itertuples():
                if itemset1[1].issubset(row[1]) and set(row[2]) == set(itemset1[2]):
                    is_subsumption = True
                    break
            # append to result if element not subsumed
            if not is_subsumption:
                self.result = self.result.append({'item': itemset1[1], 
                                                'tid': itemset1[2], 
                                                'support': len(itemset1[2])}, 
                                                ignore_index=True)
    
    def save_result(self, filename):
        self.result.to_csv(filename, sep='\t', columns=['item', 'support'], index=False)