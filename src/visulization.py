import pandas as pd
import matplotlib.pyplot as plt
import argparse
import seaborn as sns
from charm import DataPreprocessing

class Visulization:
    data = []

    def read_data(self, filename):
        with open(filename, 'r') as file:
            for line in file:
                if len(line.strip()) != 0:
                    line = line.strip().split(' ')
                    self.data.append(line)
        return len(self.data), len(self.data[0])

    def histogram(self, column):

        df = pd.DataFrame(self.data)
        df = df.astype(int)
        df[column].plot.hist(bins = df.shape[1], color='steelblue', edgecolor='black', linewidth=1.0) 
        plt.show()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--filename', help='Name of the data file', required=True)
    args = parser.parse_args()

    visual = Visulization()
    rows, cols = visual.read_data(args.filename)
    print("The data have %d rows and %d columns." % (rows, cols))
    check = 0
    while (check >= 0):
        print("Type the column you want to histogram (type -1 to exit): ")
        col = int(input())
        if col >= 0:
            visual.histogram(col)
        else:
            check = col

    number_of_closedItems = pd.DataFrame({
        'mushroom': [45, 18, 11, 9, 4, 1],
        'chess': [12838, 5119, 1736, 487, 119, 1],
        'connect': [10607, 5790, 2887, 1188, 322, 99]
    }, index=[50, 60, 70, 80, 90, 100])
    lines1 = number_of_closedItems.plot.line()
    lines1.set_xlabel("Minimum support (%)")
    lines1.set_ylabel("Number of closed itemsets")

    time = pd.DataFrame({
        'mushroom': [1.012, 0.640, 0.506, 0.454, 0.339, 0.273],
        'chess': [290.554, 86.943, 19.709, 4.031, 1.088, 0.173],
        'connect': [232.301, 101.844, 45.174, 15.629, 3.834, 1.122]
    }, index=[50, 60, 70, 80, 90, 100])
    lines2 = time.plot.line()
    lines2.set_xlabel("Minimum support (%)")
    lines2.set_ylabel("Processing Time")

    plt.show()