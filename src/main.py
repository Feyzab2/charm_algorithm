from charm import DataPreprocessing, CharmAlgorithm
import time


min_support = [50, 60, 70, 80, 90, 100]
filename = '.\data\mushroom.dat'

for ms in min_support:
    start_time = time.time()
    pre_processing = DataPreprocessing()

    pre_processing.read_data_file(filename)
    pre_processing.transform_data()
    frequent_itemset = pre_processing.get_frequent_items(ms)

    charm_algorithm = CharmAlgorithm(ms, pre_processing.tid_count)
    charm_algorithm.charm_extend(frequent_itemset)

    print("########################")
    print("The closed itemsets are:")
    print(charm_algorithm.result)

    output = ".\data\output.txt"
    charm_algorithm.save_result(output)

    end_time = time.time()

    print('Process on the dataset: ', filename, 'minimum support is: ', ms)
    print('Time processing: ', str(round((end_time - start_time), 3)), ' s')