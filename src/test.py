from charm import DataPreprocessing, CharmAlgorithm
import time

# print("Type the file name: ")
# filename = input()
# print("Type the min support:  ...%")
# min_sup = int(input())

filename = '.\data\mushroom.dat'
min_sup = 50

start_time = time.time()
pre_processing = DataPreprocessing()

pre_processing.read_data_file(filename)
pre_processing.transform_data()
# print(pre_processing.groupedItems)
frequent_itemset = pre_processing.get_frequent_items(min_sup)

s = frequent_itemset.tid.str.len().sort_values(ascending=False).index
items_grouped = frequent_itemset.reindex(s).reset_index(drop=True)
print(items_grouped.tid.str.len())

charm_algorithm = CharmAlgorithm(min_sup, pre_processing.tid_count)
charm_algorithm.charm_extend(frequent_itemset)

# print(charm_algorithm.result)