import igraph
from igraph import Graph, EdgeSeq
import plotly.graph_objects as go
from charm import DataPreprocessing
import pandas as pd

pre = DataPreprocessing()
filename = '.\\data\\unit_test.txt'
pre.read_data_file(filename)
pre.transform_data()

s = pre.groupedItems.tid.str.len().sort_values(ascending=False).index
items_grouped = pre.groupedItems.reindex(s).reset_index(drop=True)

print(items_grouped)

g = Graph()
vertices = len(items_grouped+1)

def add_edges(root, list_of_vertices):
    g.add_vertex(root['item'])
    for child_vertex in list_of_vertices:
        new_vertex = str(root['item'] + child_vertex['item'])
        g.add_vertex(new_vertex)
        g.add_edges((root['item'], new_vertex))

